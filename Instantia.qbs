QtApplication {
	files: ["*.cpp", "*.h"]
	cpp.cxxFlags: ["-Wreturn-type"]
	cpp.cxxLanguageVersion: "c++20"

	Depends { name: "Qt"; submodules: ["gui", "widgets"] }
}
