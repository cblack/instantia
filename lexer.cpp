#include <QDebug>
#include <QTextStream>
#include <optional>

#include "lexer.h"

struct Lexer::Private
{
	QString filename;
	QScopedPointer<QIODevice> device;
	Token lastToken = Token(Token::EndOfFile, Position{}, "");
	std::optional<Token> synthToken;
	std::optional<Token> peek;

	int col = 0;
	int prevCol = 0;
	int line = 1;
};

Lexer::Lexer(QString filename, std::unique_ptr<QIODevice> dev) : d(new Private)
{
	d->filename = filename;
	d->device.reset(dev.release());
}

Lexer::~Lexer()
{

}

QString Lexer::readUntilNewline()
{
	QString it;
	it.reserve(10);

	auto c = readChar();
	while (c != '\n') {
		it += c;
		c = readChar();
	}
	unreadChar(c);

	return it;
}

Token Lexer::scanIdent(char first, Position p)
{
	QString it;
	it.reserve(10);

	it += first;

	char next = readChar();
	while (QChar(next).isLetter()) {
		it += next;
		next = readChar();
	}
	unreadChar(next);

	return Token(Token::Ident, p, it);
}

char Lexer::readChar()
{
	char it = -1;
	d->device->getChar(&it);
	if (it == '\n') {
		d->prevCol = d->col;
		d->col = 0;
		d->line++;
	} else {
		d->col++;
	}
	return it;
}

void Lexer::unreadChar(char it)
{
	if (it == '\n') {
		d->col = d->prevCol;
		d->line--;
	} else {
		d->col--;
	}
	d->device->ungetChar(it);
}

Position Lexer::p()
{
	return Position{d->filename, d->col, d->line};
}

void Lexer::skipWhitespace()
{
	auto pos = p();
	char next = readChar();
	while (QChar(next).isSpace()) {
		if (next == '\n') {
			if (d->lastToken.content == "}" || d->lastToken.kind == Token::String) {
				pos.column++;
				d->synthToken = Token(Token::EndOfStatement, pos, ";");
			}
		}
		pos = p();
		next = readChar();
	}
	unreadChar(next);
}

const Token& Lexer::slast(const Token& slast)
{
	d->lastToken = slast;
	return slast;
}

Token Lexer::peek()
{
	if (d->peek.has_value())
		return d->peek.value();

	d->peek = lex();
	return d->peek.value();
}

Token Lexer::lex()
{
	if (d->peek.has_value()) {
		auto ret = d->peek.value();
		d->peek = {};
		return ret;
	}

	skipWhitespace();

	if (d->synthToken.has_value()) {
		auto it = d->synthToken.value();
		d->synthToken.reset();
		return it;
	}

	char fst = readChar();

	auto pos = p();

	QChar first(fst);

	if (first.isLetter())
	{
		return slast(scanIdent(fst, pos));
	}
	else if (fst == -1)
	{
		return slast(Token(Token::EndOfFile, pos, ""));
	}
	else
	{
		switch (first.unicode()) {
		case '{':
			return slast(Token(Token::Punct, pos, "{"));
		case '}':
			return slast(Token(Token::Punct, pos, "}"));
		case '=':
			return slast(Token(Token::Punct, pos, "="));
		case '#': {
			auto rest = readUntilNewline();
			if (elideComments) {
				return lex();
			} else {
				return slast(Token(Token::Comment, pos, '#' + rest));
			}
		}
		case '"': {
			QString next;
			next.reserve(10);
			char cont;
			cont = readChar();
			while (cont != '"') {
				next += cont;
				cont = readChar();
			}
			return slast(Token(Token::String, pos, next));
		}
		default:
			if (d->device->atEnd())
				return slast(Token(Token::EndOfFile, pos, ""));

			qWarning() << first;
			qFatal("unhandled");
		}
	}
}
