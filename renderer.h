#include <QTextDocument>
#include <QTextCursor>
#include <QTextList>

#include "parser.h"

inline void renderNode(QTextCursor to, Node node)
{
	if (node.kind == "list") {
		const bool ordered = node.attributes.value("ordered", "false") == "true";

		to.insertList(ordered ? QTextListFormat::ListDecimal : QTextListFormat::ListDisc);

		int i = 0;
		for (const auto& it : node.children) {
			i++;
			QTextCursor cp = to;

			renderNode(cp, it);

			if (i+1 != node.children.length()) {
				to.insertBlock();
			}
		}
	} else if (node.kind == "link") {
		const auto path = node.attributes.value("path");
		QTextCharFormat fmt;
		fmt.setAnchorHref(path);

		to.insertText(path, fmt);
	} else {
		qFatal("unhandled node kind");
	}
}