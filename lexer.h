#pragma once

#include <QDebug>
#include <QIODevice>
#include <memory>

struct Position
{
	QString filename;
	// 1 indexed
	int column;
	// 1 indexed
	int line;
};

inline QDebug operator<<(QDebug dbg, const Position& pos)
{
	QDebugStateSaver saver(dbg);

	dbg.nospace().noquote() << pos.filename << ":" << pos.line << ":" << pos.column;

	return dbg;
}

struct Token
{
	enum Kind {
		Ident,
		Punct,
		EndOfFile,
		Comment,
		String,
		EndOfStatement
	};

	Kind kind;
	Position pos;
	QString content;

	Token(Kind kind, Position pos, QString cont) : kind(kind), pos(pos), content(cont) {}
};

inline QDebug operator<<(QDebug dbg, Token::Kind kind)
{
	QDebugStateSaver saver(dbg);
	switch (kind) {
	case Token::Ident:
		dbg.nospace() << "ident"; break;
	case Token::Punct:
		dbg.nospace() << "punct"; break;
	case Token::EndOfFile:
		dbg.nospace() << "eof"; break;
	case Token::Comment:
		dbg.nospace() << "comment"; break;
	case Token::String:
		dbg.nospace() << "string"; break;
	case Token::EndOfStatement:
		dbg.nospace() << "eos"; break;
	default:
		qFatal("unhandled");
	}
	return dbg;
}

inline QDebug operator<<(QDebug dbg, const Token& tok)
{
	QDebugStateSaver saver(dbg);

	dbg.nospace() << "(" << tok.kind << " " << tok.content << " " << tok.pos << ")";

	return dbg;
}

class Lexer
{

	struct Private;
	QScopedPointer<Private> d;

	Token scanIdent(char first, Position p);
	QString readUntilNewline();
	void skipWhitespace();
	char readChar();
	void unreadChar(char it);
	Position p();
	const Token& slast(const Token& slast);

public:
	bool elideComments = true;

	explicit Lexer(QString filename, std::unique_ptr<QIODevice> dev);
	~Lexer();

	Token peek();
	Token lex();
};
