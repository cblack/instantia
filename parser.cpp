#include "parser.h"

struct Parser::Private
{
	std::unique_ptr<Lexer> lexer;
};

Parser::Parser(std::unique_ptr<Lexer> lexer) : d(new Private)
{
	d->lexer = std::move(lexer);
}

Parser::~Parser()
{

}

struct Panic { Error error; };

QPair<QString, QString> parseAttribute(Lexer* lexer)
{
	auto key = lexer->lex();
	auto eq = lexer->lex();
	auto val = lexer->lex();

	if (key.kind != Token::Ident)
		throw Panic{UnexpectedTokenError{key.pos, "parsing key", Token::Ident, key.kind}};

	if (eq.content != "=")
		throw Panic{UnexpectedTokenContentError{eq.pos, "parsing equals sign", "=", eq.content}};

	if (val.kind != Token::String)
		throw Panic{UnexpectedTokenError{val.pos, "parsing value", Token::String, val.kind}};

	return {key.content, val.content};
}

std::variant<Node, Error> parseNode(Lexer* lexer);

QList<Node> parseChildren(Lexer* lexer)
{
	QList<Node> ret;

	auto peeked = lexer->peek();
	while (peeked.content != "}") {
		auto node = parseNode(lexer);
		if (std::holds_alternative<Node>(node)) {
			ret << std::get<Node>(node);
		} else {
			throw Panic{std::get<Error>(node)};
		}
		peeked = lexer->peek();
	}

	lexer->lex();

	return ret;
}

std::variant<Node, Error> parseNode(Lexer* lexer)
{
	Node node;

	auto it = lexer->lex();

	switch (it.kind) {
	case Token::Ident: {
		node.kind = it.content;
		auto peeked = lexer->peek();
		while (peeked.content != "{") {
			if (peeked.kind == Token::EndOfStatement) {
				lexer->lex(); // we know it's a ;
				return node;
			}

			auto [k, v] = parseAttribute(lexer);
			node.attributes[k] = v;

			peeked = lexer->peek();
		}
		lexer->lex(); // we know it's a {
		node.children = parseChildren(lexer);
		auto eos = lexer->lex(); // we know it's a .
		if (eos.kind != Token::EndOfStatement) {
			return UnexpectedTokenError{eos.pos, "finishing parsing node", Token::EndOfStatement, eos.kind};
		}

		return node;
	}
	default:
		return UnexpectedTokenError{it.pos, "parsing node", Token::Ident, it.kind};
	}
}

std::variant<Node, Error> Parser::parse()
{
	try {
		return parseNode(d->lexer.get());
	} catch (Panic p) {
		return p.error;
	}
}
