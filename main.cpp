#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QTextDocument>
#include <QTextBrowser>

#include "parser.h"
#include "lexer.h"
#include "renderer.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);

	auto file = new QFile(app.arguments()[1]);
	if (!file->open(QIODevice::ReadOnly)) {
		qFatal("error opening file");
	}

	Parser parser(std::make_unique<Lexer>(app.arguments()[1], (std::unique_ptr<QFile>(file))));

	auto parsed = parser.parse();
	if (std::holds_alternative<Error>(parsed)) {
		qWarning() << parsed;
		return -1;
	}

	QTextBrowser browser;
	QTextCursor root(browser.document());

	auto node = std::get<Node>(parsed);
	renderNode(root, node);

	browser.show();

	return app.exec();
}

