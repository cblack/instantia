#pragma once

#include <QScopedPointer>

#include "lexer.h"

struct Node
{
	QString kind;
	QMap<QString,QString> attributes;
	QList<Node> children;
};

inline QDebug operator<<(QDebug dbg, const Node& node)
{
	QDebugStateSaver saver(dbg);

	dbg.nospace() << "(" << node.kind << " " << node.attributes << " " << node.children << ")";

	return dbg;
}

struct UnexpectedTokenError
{
	Position at;
	QString context;
	Token::Kind expected;
	Token::Kind got;
};

inline QDebug operator<<(QDebug dbg, const UnexpectedTokenError& node)
{
	QDebugStateSaver saver(dbg);

	dbg.nospace() << "(ERROR at " << node.at << ": expected " << node.expected << " while " << node.context << ", got " << node.got << ")";

	return dbg;
}

struct UnexpectedTokenContentError
{
	Position at;
	QString context;
	QString expected;
	QString got;
};

inline QDebug operator<<(QDebug dbg, const UnexpectedTokenContentError& node)
{
	QDebugStateSaver saver(dbg);

	dbg.nospace() << "(ERROR at " << node.at << ": expected " << node.expected << " while " << node.context << ", got " << node.got << ")";

	return dbg;
}

using Error = std::variant<UnexpectedTokenError, UnexpectedTokenContentError>;

inline QDebug operator<<(QDebug dbg, const Error& node)
{
	QDebugStateSaver saver(dbg);

	if (std::holds_alternative<UnexpectedTokenError>(node)) {
		dbg.nospace() << std::get<UnexpectedTokenError>(node);
	} else if (std::holds_alternative<UnexpectedTokenContentError>(node)) {
		dbg.nospace() << std::get<UnexpectedTokenContentError>(node);
	}

	return dbg;
}

inline QDebug operator<<(QDebug dbg, const std::variant<Node, Error>& node)
{
	QDebugStateSaver saver(dbg);

	if (std::holds_alternative<Node>(node)) {
		dbg.nospace() << std::get<Node>(node);
	} else {
		dbg.nospace() << std::get<Error>(node);
	}

	return dbg;
}

class Parser
{

	struct Private;
	QScopedPointer<Private> d;

public:
	explicit Parser(std::unique_ptr<Lexer> lexer);
	~Parser();

	std::variant<Node, Error> parse();

};
